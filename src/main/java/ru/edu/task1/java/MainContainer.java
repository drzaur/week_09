package ru.edu.task1.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class MainContainer {

    private ComponentA componentA;

    private ComponentB componentB;

    public boolean isValid() {
        return componentA != null && componentB != null && componentA.isValid() && componentB.isValid();
    }
}
